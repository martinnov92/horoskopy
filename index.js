const fs = require('fs');
const axios = require('axios');
const cheerio = require('cheerio');

const TOKEN = process.env.TOKEN;
const repositoryFilePath = 'https://gitlab.com/api/v4/projects/16401844/repository/files';

const signs = {
    aries: 'beran',
    taurus: 'byk',
    gemini: 'blizenci',
    cancer: 'rak',
    leo: 'lev',
    virgo: 'panna',
    libra: 'vahy',
    scorpio: 'stir',
    sagittarius: 'strelec',
    capricorn: 'kozoroh',
    aquarius: 'vodnar',
    pisces: 'ryby',
}

const otherContentsTitle = [
     'love',
     'work',
     'relationships',
     'health',
     'tips',
];

// post -> new file
// put -> update file
function commitFile(fileName, method = 'post') {
    const date = new Date();
    const content = fs.readFileSync(fileName, { encoding: 'base64' });

    return axios({
        method,
        url: `${repositoryFilePath}/${encodeURIComponent(fileName)}`,
        responseType: 'application/json',
        headers: {
            'PRIVATE-TOKEN': TOKEN,
        },
        data: {
            content,
            branch: 'master',
            encoding: 'base64',
            author_name: 'Martin Novák',
            author_email: 'martinnovak@outlook.com',
            commit_message: `Scrape and save new horoscopes - ${date.toISOString()}`,
        },
    });
}

async function getHoroscope(sign) {
    const response = await axios(`http://horoskopy.cz/${sign}`);
    const html = response.data;

    const $ = cheerio.load(html);
    const contentDetail = $('#content-detail');

    const paragraphs = contentDetail.find('.brown').next('p');
    const horoscope = {
        main: contentDetail.find('h2').next('p').text().trim(),
    };

    if (paragraphs.length) {
        paragraphs.each((i, node) => {
            horoscope[otherContentsTitle[i]] = node.firstChild.data.trim();
        });
    }

    return horoscope;
}

async function init() {
    const dailyHoroscope = {};

    for (const [en, cs] of Object.entries(signs)) {
        dailyHoroscope[en] = await getHoroscope(cs);
    }

    // save file to folder /year/month/day.json
    const date = new Date();
    const year = date.getFullYear();

    let day = date.getDate();
    let month = date.getMonth() + 1;

    day = day < 10 ? `0${day}` : day;
    month = month < 10 ? `0${month}` : month;

    const fileName = `${year}/${month}/${day}.json`;

    fs.mkdir(`${year}`, () => {
        fs.mkdir(`${year}/${month}`, () => {
            fs.writeFile(fileName, JSON.stringify(dailyHoroscope, null, 4), async () => {
                console.log('FILE SAVED TO DISK');

                try {
                    // commit new file
                    await commitFile(fileName);

                    console.log('NEW FILE PUSHED TO GIT');
                } catch (exception) {
                    console.log('TRYING TO UPDATE EXISTING FILE');

                    try{
                        // commit (update) existing file
                        await commitFile(fileName, 'put');

                        console.log('EXISTING FILE UPDATED IN GIT');
                    } catch (innerException) {
                        console.log('TOTAL FAIL', innerException);
                    }
                }

            });
        });
    });
}

init();
