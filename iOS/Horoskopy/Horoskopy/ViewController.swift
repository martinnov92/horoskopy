//
//  ViewController.swift
//  Horoskopy
//
//  Created by Martin Novák on 19/01/2020.
//  Copyright © 2020 Martin Novák. All rights reserved.
//

import UIKit
import SQLite3

class ViewController: UIViewController {

    var signTitle: String = ""
    var horoscope: [String: String] = [:]

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var mainContent: UILabel!
    @IBOutlet weak var loveContent: UILabel!
    @IBOutlet weak var workContent: UILabel!
    @IBOutlet weak var healthContent: UILabel!
    @IBOutlet weak var horoscopeSignLabel: UILabel!
    @IBOutlet weak var relationshipsContent: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        let now = Date()
        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.locale = Locale(identifier: "cs_CZ")

        // date and sign
        horoscopeSignLabel.text = signTitle
        dateLabel.text = dateFormatter.string(from: now)
        
        // horoscope details
        loveContent.text = horoscope["love"] ?? ""
        mainContent.text = horoscope["main"] ?? ""
        workContent.text = horoscope["work"] ?? ""
        healthContent.text = horoscope["health"] ?? ""
        relationshipsContent.text = horoscope["relationships"] ?? ""
    }

}

