//
//  SelectSignViewController.swift
//  Horoskopy
//
//  Created by Martin Novák on 22/01/2020.
//  Copyright © 2020 Martin Novák. All rights reserved.
//

import Foundation
import UIKit

class SelectSignViewController: UIViewController {

    @IBAction func handleButtonTouchUpInside(_ sender: UIButton) {
        if let title = sender.configuration?.subtitle {
            self.performSegue(withIdentifier: "SelectSignViewControllerToDetail", sender: title)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        Horoscope.getTodayHoroscopes()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "SelectSignViewControllerToDetail"){
            let vc = segue.destination as! ViewController

            if sender != nil {
                let title = sender as! String
                let sign = signsCsToEn[title]

                if let horoscopeForSign = Horoscope.horoscopes[sign!] {
                    vc.signTitle = title
                    vc.horoscope = horoscopeForSign
                }
            }
        }
    }

}
