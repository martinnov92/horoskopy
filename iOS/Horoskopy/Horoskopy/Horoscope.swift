//
//  Horoscope.swift
//  Horoskopy
//
//  Created by Martin Novak on 03.05.2022.
//  Copyright © 2022 Martin Novák. All rights reserved.
//

import Foundation

class Horoscope {

    static var horoscopes: [String: [String: String]] = [:]

    static func getTodayHoroscopes() {
        // DB URL
        let now = Date()
        let jsonFileUrl = "https://gitlab.com/martinnov92/horoskopy/-/raw/master/\(now.year)/\(now.month)/\(now.date).json"

        URLSession.shared.dataTask(with: URL(string: jsonFileUrl)!) { (data, response, error) in
            if error == nil && data != nil {
                do {
                    // Convert to dictionary where keys are of type String, and values are of any type
                    let json = try JSONSerialization.jsonObject(with: data!, options: [.mutableContainers, .allowFragments]) as! [String: [String: String]]

                    DispatchQueue.main.async {
                        horoscopes = json
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }

}
