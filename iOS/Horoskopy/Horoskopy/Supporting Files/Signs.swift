//
//  Signs.swift
//  Horoskopy
//
//  Created by Martin Novak on 03.05.2022.
//  Copyright © 2022 Martin Novák. All rights reserved.
//

import Foundation

let signs = [
    "aries": "♈️ Beran",
    "taurus": "♉️ Býk",
    "gemini": "♊️ Blíženci",
    "cancer": "♋️ Rak",
    "leo": "♌️ Lev",
    "virgo": "♍️ Panna",
    "libra": "♎️ Váhy",
    "scorpio": "♏️ Štír",
    "sagittarius": "♐️ Střelec",
    "capricorn": "♑️ Kozoroh",
    "aquarius": "♒️ Vodnář",
    "pisces": "♓️ Ryby",
]


let signsCsToEn = [
    "Beran": "aries",
    "Býk": "taurus",
    "Blíženci": "gemini",
    "Rak": "cancer",
    "Lev": "leo",
    "Panna": "virgo",
    "Váhy": "libra",
    "Štír": "scorpio",
    "Střelec": "sagittarius",
    "Kozoroh": "capricorn",
    "Vodnář": "aquarius",
    "Ryby": "pisces",
]
