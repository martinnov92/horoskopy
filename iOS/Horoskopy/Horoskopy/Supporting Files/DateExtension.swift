//
//  DateExtension.swift
//  Horoskopy
//
//  Created by Martin Novak on 03.05.2022.
//  Copyright © 2022 Martin Novák. All rights reserved.
//

import Foundation

extension Date {
    var month: String {
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "MM"

        return dateFormatter.string(from: self)
    }
    
    var date: String {
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "dd"

        return dateFormatter.string(from: self)
    }
    
    var year: String {
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "YYYY"

        return dateFormatter.string(from: self)
    }
}

